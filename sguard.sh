#!/usr/bin/env bash

# Checks ----------------------------------------------------------------------

# is pacmd installed?
if [[ -z `whereis pw-dump | awk '{ print $2 }'` ]]; then
    echo "pw-dump from pipewire-utils is required for sguard to function properly."
    exit 1;
fi

# is gnome-session-inhibit installed?
if [[ -z `whereis gnome-session-inhibit | awk '{ print $2 }'` ]]; then
    echo "gnome-session-inhibit is required for sguard to function properly."
    exit 2;
fi

# Configuration ---------------------------------------------------------------
if [[ -z $DEBUG ]]; then
    DEBUG="false"
fi
if [[ -z $TIMEOUT ]]; then
    TIMEOUT=60
fi

# Functions -------------------------------------------------------------------

# check player state.
function check_player_state {
    if [[ -z `pw-dump -N | grep '"state": "running",$'` ]]; then
        echo false
    else
        echo true
    fi
}

# prevent sleep temporary (Timeout + 1 sec)
function prevent_sleep {
    gnome-session-inhibit --inhibit suspend \
                          --reason "Audio is playing..." \
                          sleep $(($TIMEOUT + 1))
}

# Execution -------------------------------------------------------------------

while :; do
    if [[ `check_player_state` == "true" ]]; then
        prevent_sleep &
        if [[ "$DEBUG" == "true" ]]; then
            echo "inhibit sleep for $TIMEOUT seconds"
        fi
    fi
    sleep $TIMEOUT # Zzzz...
done

# EOF sguard.sh
