install:
	mkdir -p ${HOME}/.local/bin
	mkdir -p ${HOME}/.config/systemd/user
	install -m 700 ./sguard.sh ${HOME}/.local/bin/
	install -m 644 ./sguard.service ${HOME}/.config/systemd/user/
	systemctl --user daemon-reload
