# Suspend Guard for Phosh

## What does it do?

Prevent auto-suspend as long as media (audio) is playing, e.g. from your
podcast- or music-player.

Some players (like `lollypop`) have built-in suspend-inhibit, but not all of
them have yet (e.g. `gnome-podcasts`) and some probably never will
(e.g. `firefox`).

## How does it work?

The `sguard.sh` script checks every X seconds (default: 60), whether sound is
playing through any pipewire-audio channel. If it is, it will inhibit suspend for
the next X+1 seconds, until it checks again.

## Are there system requirements?

Yes, the script uses `pw-dump`, which is probably installed anyways and
`gnome-session-inhibit` which is part of the GNOME desktop and therefore part of
phosh.

## Wouldn't it be better to check mpris for the current player state?

Indeed, that would be a _cleaner_ approach, and the [script did exactly that for
a while](https://src.jayvii.de/Hobby/PinePhoneScripts/src/commit/ea05e205a1315464bb9ebda0ad86493eb844e395/sguard/sguard.sh).
However, it turns out that phosh handles MediaPlayer/mpris events in an odd way,
such that it does not report any playing state, when the screen is locked.

Additionally, some applications do not support mpris and would therefore be
unable to trigger the suspend-inhibit.

## Use sguard

You may need to install pipewire-utils

```bash
sudo dnf install pipewire-utils
```

Just download this source directory, and run the Makefile:
```
git clone https://gitlab.com/Baggypants/sguard-fedora.git
cd sguard-fedora
make install
```

This will place `sguard.sh` into `~/.local/bin/sguard.sh` and place the
systemd-service file under your user's directory. You can enable & start sguard
with:
```
systemctl --user enable sguard --now
```

## Upstream Repository

https://src.jayvii.de/Hobby/PinePhoneScripts
